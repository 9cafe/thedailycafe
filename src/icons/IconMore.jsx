import React from 'react';

import Icon from '../components/Icon';

function IconMore(props) {
	return (
		<Icon viewBox='0 0 19 19' width='19' height='19' fillColor={'grey2'} {...props}>
			<path
				d='M9.5 8c.8 0 1.5.7 1.5 1.5S10.3 11 9.5 11 8 10.3 8 9.5 8.7 8 9.5 8zM5.1 8c.8 0 1.5.7 1.5 1.5S5.9 11 5.1 11s-1.5-.7-1.5-1.5S4.3 8 5.1 8zm8.8 0c.8 0 1.5.7 1.5 1.5s-.7 1.5-1.5 1.5-1.5-.7-1.5-1.5.7-1.5 1.5-1.5zM9.5 0C4.2 0 0 4.2 0 9.5S4.2 19 9.5 19 19 14.7 19 9.5 14.7 0 9.5 0zm0 17.5c-4.4 0-8-3.6-8-8s3.6-8 8-8 8 3.6 8 8-3.6 8-8 8z'
				fill='#cacfd6'
			/>
		</Icon>
	);
}

export default IconMore;
