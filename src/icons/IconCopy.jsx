import React from 'react';
import styled from 'styled-components';

import Icon from '../components/Icon';
import color from '../styles/color';

const Svg = styled(Icon)`
  height: 100%;
  &:hover {
    fill: ${color.brand};
  }
`
function IconCopy(props) {
  return (
    <Svg viewBox="-4 -4 28 28" width={28} height={28} fillColor={'grey2'} {...props} >
    <g transform="scale(0.70)">
      <path
        d="M23.2 3.2h-12c-.9 0-1.5.7-1.5 1.5v3.6h7c1.6 0 3 1.3 3 3v6.9h3.5c.9 0 1.5-.7 1.5-1.5v-12c.1-.8-.6-1.5-1.5-1.5z"
      />
      <path
        d="M
        16.7 9.8h-12c-.9 0-1.5.7-1.5 1.5v12c0 .9.7 1.5 1.5 1.5h12c.9 0 1.5-.7 1.5-1.5v-12c.1-.8-.6-1.5-1.5-1.5z"
      />
      </g>
    </Svg>
  )
}

export default IconCopy;
