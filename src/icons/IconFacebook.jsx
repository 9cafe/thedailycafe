import React from 'react';
import styled from 'styled-components';

import Icon from '../components/Icon';
import color from '../styles/color';

const Svg = styled(Icon)`
  height: 100%;
  &:hover {
    fill: ${color.brand};
  }
`

function IconFacebook(props) {
  return (
    <Svg viewBox="-3.5 -3.5 28 28" width="100" height="100" fillColor={'grey2'} {...props} >
      <path
        d="M11.6 10.9H9.3V14h2.4v9.4h3.9V14h2.9l.3-3.1h-3.1V9.6c0-.7.2-1 .9-1h2.3v-4h-3c-2.8 0-4.1 1.2-4.1 3.6v2.7z"
        transform="scale(0.75)"
      />
    </Svg>
  )
}

export default IconFacebook;
