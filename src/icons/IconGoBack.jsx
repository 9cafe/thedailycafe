import React from 'react';
import styled from 'styled-components';

import Icon from '../components/Icon';

export const Custom = styled(Icon)`
	padding-right: 1rem;
`;

function IconGoBack(props) {
	return (
		<Custom viewBox='0 0 23.8 16.8' width='23' height='23' {...props}>
			<path
				d='M7.8,0.3c0.3-0.3,0.9-0.3,1.2,0c0.3,0.3,0.3,0.8,0,1.2L2.9,7.5H23c0.5,0,0.8,0.4,0.9,0.8
        c0,0,0,0,0,0c0,0.5-0.4,0.9-0.9,0.9H2.9L9,15.3c0.3,0.3,0.3,0.9,0,1.2c-0.3,0.3-0.9,0.3-1.2,0c0,0,0,0,0,0L0.2,9
        c-0.3-0.3-0.3-0.9,0-1.2L7.8,0.3z'
			/>
		</Custom>
	);
}

export default IconGoBack;
