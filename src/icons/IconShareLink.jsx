import React from 'react';

import Icon from '../components/Icon';

function IconShareLink(props) {
	return (
		<Icon viewBox='0 0 20 20' width='19' height='19' fillColor={'grey2'} {...props}>
			<g>
				<path d='M15 9h-2V7a3 3 0 10-6 0v2H5V7a5 5 0 0110 0v2zM13 13v-2h2v2a5 5 0 01-10 0v-2h2v2a3 3 0 106 0z'></path>
				<path d='M11 7H9v6h2V7z'></path>
			</g>
		</Icon>
	);
}

export default IconShareLink;
