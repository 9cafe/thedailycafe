import React from 'react';
import Icon from '../components/Icon';

function IconBusiness(props) {
	return (
		<Icon
			viewBox='0 0 24 24'
			width={24}
			height={21}
			fillColor={'black'}
			verticalAlign={'text-top'}
			{...props}
		>
			<path d='M24 21.4h-1.8v-1.8h-.8v-8.8h.8V9H24V6.4L12 0 0 6.4V9h1.8v1.8h.8v8.8h-.8v1.8H0V24h24v-2.6zm-2.5-1v1.1h-2.6v-1.1h2.6zm-3.4 1h-1.4v-1.8H16v-8.8h.8V9h1.4v1.8h.8v8.8h-.8l-.1 1.8zm-5.4 0h-1.4v-1.8h-.8v-8.8h.8V9h1.4v1.8h.8v8.8h-.8v1.8zm-5.5 0H5.8v-1.8h-.7v-8.8h.8V9h1.4v1.8H8v8.8h-.8v1.8zM8 10.1V9h2.6v1.1H8zm1.8 9.5h-1v-8.8h1v8.8zm.7.7v1.2H7.9v-1.1l2.6-.1zm2.9-10.2V9H16v1.1h-2.6zm1.8 9.5h-1v-8.8h1v8.8zm.8.8v1.1h-2.6v-1.1H16zm3.7-9.6h1v8.8h-1v-8.8zm-.8-.7V9h2.6v1.1h-2.6zM.7 8.2V6.9l11.2-6 11.2 6v1.3H.7zm1.8 1.9V9h2.6v1.1H2.5zm1.8 9.5h-1v-8.8h1v8.8zm.8.8v1.1H2.5v-1.1h2.6zM.7 23.3v-1.1h22.5v1.1H.7zM9.1 5h.7v.7h-.7V5zm2.5 0h.7v.7h-.7V5zm2.5 0h.7v.7h-.7V5z' />
		</Icon>
	);
}

export default IconBusiness;
