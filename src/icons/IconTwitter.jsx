import React from 'react';
import styled from 'styled-components';

import Icon from '../components/Icon';
import color from '../styles/color';

const Svg = styled(Icon)`
  height: 100%;
  &:hover {
    fill: ${color.brand};
  }
`
function IconTwitter(props) {
  return (
    <Svg viewBox="-5 -6 28 28" width="100" height="10" fillColor={'grey2'} {...props} >
      <path
        d="M26 6.2c-.9.4-1.8.7-2.8.8 1-.6 1.8-1.6 2.2-2.7-1 .6-2 1-3.1 1.2-.9-1-2.2-1.6-3.6-1.6-3.2 0-5.5 3-4.8 6-4.2-.2-7.8-2.1-10.2-5.1C2.4 7 3 9.9 5.2 11.4c-.8 0-1.6-.2-2.2-.6-.1 2.3 1.6 4.4 3.9 4.9-.7.2-1.5.2-2.2.1.6 2 2.4 3.4 4.6 3.4-2.1 1.6-4.7 2.3-7.3 2 2.2 1.4 4.8 2.2 7.5 2.2 9.1 0 14.3-7.7 14-14.6 1-.7 1.8-1.6 2.5-2.6z"
        transform="scale(0.6)"
      />
    </Svg>
  )
}

export default IconTwitter;
