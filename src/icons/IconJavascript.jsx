import React from 'react';

import Icon from '../components/Icon';

function IconJavascript(props) {
	return (
		<Icon viewBox='0 0 50 50' width='50' height='50' fillColor={'brand'} {...props}>
			<path d='M0 0v50h50V0zm26.52 38.85c0 5.1-3 7.41-7.35 7.41a7.61 7.61 0 01-7.38-4.5l4-2.42c.77 1.38 1.47 2.54 3.16 2.54s2.64-.64 2.64-3.1V22.05h4.92zm11.63 7.43c-4.57 0-7.52-2.18-9-5l4-2.32a5.37 5.37 0 004.86 3c2 0 3.34-1 3.34-2.43 0-1.69-1.34-2.29-3.58-3.27l-1.23-.53c-3.56-1.51-5.91-3.4-5.91-7.41 0-3.69 2.81-6.5 7.2-6.5a7.27 7.27 0 017 3.94L41 28.17a3.36 3.36 0 00-3.17-2.11A2.14 2.14 0 0035.52 28a1.21 1.21 0 000 .19c0 1.48.91 2.07 3 3l1.23.53c4.19 1.79 6.55 3.62 6.55 7.73.01 4.4-3.47 6.83-8.15 6.83z' />
		</Icon>
	);
}

export default IconJavascript;
