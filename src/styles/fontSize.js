export default {
	tiny: '0.4',
	small: '.8',
	initial: '1.13',
	medium: '1.5'
};
