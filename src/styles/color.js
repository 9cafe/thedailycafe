export default {
  'brand': "#443caa",
  'brandbg': "#E4E9F0",
  'pink': "#D12856",
  'pinkbg': "#F0E4E8",
  'black': "#000000",
  'white': "#FFFFFF",
  'grey': "#ACB0B5",
  'grey2': "#cacfd6"
}