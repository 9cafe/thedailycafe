export default {
	fontSize: {
		xs: '.4',
		sm: '.8',
		md: '1',
		lg: '1.13',
		lg2: '1.3',
		xl: '1.6'
	},
	fontFamily: {
		sans: '"CircularStd", Helvetica, Arial, sans-serif',
		serif: '"Times New Roman", serif'
	},
	fontWeight: {
		normal: 300,
		medium: 500,
		bold: 700
	},
	color: {
		inherit: 'inherit',
		brand: '#443caa',
		brandbg: '#E4E9F0',
		pink: '#D12856',
		pinkbg: '#F0E4E8',
		black: '#000000',
		white: '#FFFFFF',
		grey1: '#878695',
		grey2: '#ACB0B5',
		grey3: '#cacfd6',
		grey4: '#F0F4F6',
		grey5: '#E7E6F5'
	},
	spacing: {
		xs: 0.5,
		sm: 1,
		md: 1.2,
		lg: 1.5,
		xl: 1.9
	},
	lineHeight: {
		xs: 0.7,
		sm: 1,
		md: 1.5,
		lg: 1.8,
		xl: 2,
		xl2: 3
	},
	transition: {
		xs: '.2s',
		sm: '.3s'
	}
};
