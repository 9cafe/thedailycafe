import styled from 'styled-components';

const GridCell = styled.div`
  position: relative;
  grid-column: ${({ column }) => column};
  height: ${({ fullHeight }) => (fullHeight ? '100%' : 'auto')};
  min-width: ${({ minWidth }) => minWidth};
`;

GridCell.defaultProps = {
  column: 'auto',
  minWidth: 'auto',
};

export default GridCell;
