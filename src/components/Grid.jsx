import styled from 'styled-components';

const Grid = styled.div`
	display: grid;
	align-items: ${({ align }) => align};
	direction: ${({ direction }) => direction};
	justify-content: ${({ justify }) => justify};
	grid-column-gap: ${({ gap }) => gap}px;
	grid-template-columns: ${({ columns }) => columns};
	height: ${({ fullHeight }) => (fullHeight ? '100%' : 'auto')};
`;

Grid.defaultProps = {
	gap: 0,
	align: 'center',
	columns: '50% 50%',
	direction: 'ltr',
	justify: 'start'
};

export default Grid;
