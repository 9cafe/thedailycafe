import styled from 'styled-components';

import global from '../styles/global';

const ExternalLink = styled.a`
  display: ${({ textDisplay }) => textDisplay};
  cursor: pointer;
  text-align: ${({ align }) => align};
  text-decoration: none;
  color: ${({ textColor }) => global.color[textColor]};
  font-size: ${({ fontSize }) => global.fontSize[fontSize] + 'em'};
  font-weight: ${({ fontWeight }) => global.fontWeight[fontWeight]};
  font-family: inherit;
  opacity: ${({ textOpacity }) => textOpacity};
  line-height: ${({ lineHeight }) => lineHeight + 'rem'};
  margin: ${({ containerMargin }) => containerMargin};
  &:hover {
    color: ${global.color.pink} !important;
  }
  &:visited {
    color: ${global.color.brand};
  }
`;

ExternalLink.defaultProps = {
  align: 'center',
  textDisplay: 'flex'
};

export default ExternalLink;
