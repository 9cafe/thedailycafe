import styled from 'styled-components';

export const Input = styled.input`
	border: none;
	outline: none;

	display: block;
	width: 10rem;
	border: 2px solid rgba(0, 0, 0, 0.1);
	border-radius: 5px;
	/* padding: 0 0.3em; */
	margin-top: 0.5em;
	text-align: center;
	background: rgba(0, 0, 0, 0.1);

	font-size: ${global.fontSize.small}em;
	line-height: 2em;

	&:hover {
		border: 2px solid ${global.color.brand};
	}
`;
