import styled from 'styled-components';

import global from '../styles/global';

const Text = styled.p`
	display: block;
	text-align: ${({ align }) => align};
	color: ${({ textColor }) => global.color[textColor]};
	font-size: ${({ fontSize }) => global.fontSize[fontSize] + 'em'};
	font-weight: ${({ fontWeight }) => global.fontWeight[fontWeight]};
	font-family: ${({ display }) => global.fontFamily[display]};
	opacity: ${({ textOpacity }) => textOpacity};
	line-height: ${({ lineHeight }) => lineHeight + 'rem'};
  margin: ${({ containerMargin }) => containerMargin}
`;

Text.defaultProps = {
	align: 'left',
	display: 'sans',
	textColor: 'black',
	fontWeight: 'normal',
	fontSize: 'lg'
};

export default Text;
