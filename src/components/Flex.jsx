import styled from 'styled-components';

const Flex = styled.div`
  display: flex;
  min-width: ${({ minWidth }) => minWidth};
  align-items: ${({ align }) => align};
  flex-direction: ${({ direction }) => direction};
  justify-content: ${({ justify }) => justify};
  margin: ${({ containerMargin }) => containerMargin};
  padding: ${({ containerPadding }) => containerPadding};
`;

Flex.defaultProps = {
  align: 'start',
  direction: 'row',
  justify: 'start',
  minWidth: 'auto'
};

export default Flex;
