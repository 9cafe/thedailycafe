import React, { memo } from 'react';

import Flex from 'components/Flex';
import { Title, Description, Input, Tooltip } from './partials';
import { IsMobile, Dot } from 'components/layout/Article/partials';

import { motion } from 'framer-motion';

const variants = {
  open: {
    y: 0,
    opacity: 1,
    transition: {
      y: { stiffness: 1000, velocity: -100 }
    }
  },
  closed: {
    y: 50,
    opacity: 0,
    transition: {
      y: { stiffness: 1000 }
    }
  }
};

function ArticleLink(props) {
  const {
    articleId,
    linkId,
    children,
    link,
    description,
    copyToClipboard,
    tooltipText,
    handleTooltip,
    handleClick
  } = props;

  const handleCopy = event => {
    event.target.select();
    document.execCommand('copy');

    handleTooltip();
  };

  return (
    <motion.div variants={variants}>
      <Flex>
        <IsMobile>
          <Dot />
        </IsMobile>
        <Title href={link} target='_blank' onClick={e => handleClick(linkId, articleId)}>
          {children}
        </Title>
      </Flex>
      {description && <Description>{description}</Description>}
      {copyToClipboard[link] && (
        <Flex align='center'>
          <Input onClick={handleCopy} readOnly type='text' value={link} />
          <Tooltip tooltipText={tooltipText}>Copied to clipboard!</Tooltip>
        </Flex>
      )}
    </motion.div>
  );
}

export default memo(ArticleLink);
