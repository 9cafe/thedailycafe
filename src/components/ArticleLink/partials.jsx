import styled, { css } from 'styled-components';

import { device } from 'styles/devices';
import global from 'styles/global';

import IconShareLink from 'icons/svg/IconShareLink.svg';

export const Link = styled.a``;

export const Title = styled.a`
	display: inline-block;
	max-width: 100%;
	color: ${global.color.brand};
	cursor: pointer;
	opacity: inherit;

	font-family: ${global.fontFamily.serif};
	font-size: ${global.fontSize.xl}em;
	line-height: ${global.lineHeight.xl}rem;
	padding: 0.3em 0 0.3em 0;

	&:hover {
		color: ${global.color.pink};
	}
	&:link {
		color: ${global.color.brand};
	}
	&:visited {
		color: #b2b0cc;
	}

	@media ${device.tablet} {
		max-width: 32em;
	}
`;

export const Description = styled.p`
	max-width: 100%;
	color: ${global.color.grey1};
	margin-left: ${global.spacing.sm + 0.6}em;
	line-height: ${global.spacing.sm + 0.7}em;

	font-weight: 400;
	font-size: ${global.fontSize.lg}em;
	letter-spacing: 0.3px;
	line-height: ${global.lineHeight.sm}em;

	@media ${device.tablet} {
		margin-left: 0;
		max-width: 32em;
		margin-top: 0.2em;
		line-height: ${global.lineHeight.lg}em;
	}
`;

export const Input = styled.input`
	display: block;
	border: none;
	outline: none;

	width: 11rem;
	border: 2px solid ${global.color.grey4};
	border-radius: 5px;
	margin-top: ${global.spacing.xs}em;

	background: url(${IconShareLink}) ${global.color.grey4} no-repeat scroll 5px 5px;
	padding-left: 40px;

	font-size: ${global.fontSize.sm}em;
	height: ${global.lineHeight.xl}em;

	&:hover {
		border: 2px solid ${global.color.brand};
	}

	cursor: text;
`;

export const Tooltip = styled.p`
	margin-top: 0.5em;
	margin-left: 0.5em;
	color: ${global.color.brand};

	font-size: ${global.fontSize.sm}em;

	transition: transform 0.5s, opacity 0.5s;
	transform: translateX(0);
	opacity: 1;

	${({ tooltipText }) =>
		!tooltipText &&
		css`
			transform: translateX(15px);
			opacity: 0;
			transition: transform 0.5s, opacity 0.5s;
		`};
`;
