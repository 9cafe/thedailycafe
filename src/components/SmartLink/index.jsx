import React from 'react';
import styled from 'styled-components';

import global from '../../styles/global';

const Separator = styled.span`
	display: inline-block;
	width: 1px;
	height: 100%;
	background: ${global.color.grey2};
	opacity: 50%;
	margin: 0 ${global.spacing.sm}rem 0 ${global.spacing.sm}rem;
	vertical-align: middle;
`;
const List = styled.li`
	line-height: ${global.spacing.sm}em;

	& > a {
		color: ${global.color.brand};
		&:hover {
			color: ${global.color.pink};
		}
	}
`;

function SmartLink({ title, link, isFirst }) {
	if (isFirst) {
		return (
			<List>
				<a href={link}>{title}</a>
			</List>
		);
	}
	return (
		<List>
			<Separator />
			<a href={link}>{title}</a>
		</List>
	);
}

export default SmartLink;
