import styled from 'styled-components';
import { motion } from 'framer-motion';

const FlexMotion = styled(motion.div)`
	display: flex;
	min-width: ${({ minWidth }) => minWidth};
	align-items: ${({ align }) => align};
	flex-direction: ${({ direction }) => direction};
	justify-content: ${({ justify }) => justify};
	margin: ${({ containerMargin }) => containerMargin};
	padding: ${({ containerPadding }) => containerPadding};
`;

FlexMotion.defaultProps = {
	align: 'start',
	direction: 'row',
	justify: 'start',
	minWidth: 'auto'
};

export default FlexMotion;
