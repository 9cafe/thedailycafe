import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

import global from '../../styles/global';

import IconBusiness from '../../icons/IconBusiness';
import IconCoffee from '../../icons/IconCoffee';

const Wrapper = styled.div`
	font-size: ${global.fontSize.lg}em;
	letter-spacing: 0.01em;
	color: ${global.color.black};
	line-height: 1.6em;
	width: 100%;

	${({ topView }) => (topView ? 'opacity: 1;' : 'opacity: 0;')};
`;

const news = {
	open: { opacity: 1, y: '50%' },
	close: { opacity: 0, y: '150%' }
};

const about = {
	open: { opacity: 1, y: '-50%' },
	close: { opacity: 0, y: '-150%' }
};

function Slogan({ topView, page }) {
	return (
		<Wrapper topView={topView}>
			<motion.p animate={page ? 'close' : 'open'} variants={news} initial={false}>
				Carefully curated news to relax our mornings <IconCoffee />
			</motion.p>
			<motion.p animate={page ? 'open' : 'close'} variants={about} initial={false}>
				Carefully crafted designs to help your business <IconBusiness />
			</motion.p>
		</Wrapper>
	);
}

export default Slogan;
