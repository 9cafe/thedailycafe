import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { AnimatePresence } from 'framer-motion';

import MemoizedArticle from '../layout/Article';
import Footer from '../layout/Footer';
import Flex from '../Flex';
import Button from '../Button';
import WithLoading from '../HOC/WithLoading';

function Home({ articles, isLoaded, shouldAnimateIn, handleInAnimation, handleClick }) {
  console.log(articles);
  if (isLoaded) {
    setTimeout(() => {
      handleInAnimation();
    }, 700);
  }

  return (
    <WithLoading isLoaded={isLoaded}>
      <div>
        <AnimatePresence initial={shouldAnimateIn}>
          {articles.map(article => {
            return (
              <MemoizedArticle
                article={article}
                key={article.id}
                handleClick={handleClick}
                isLoaded={isLoaded}
              />
            );
          })}
        </AnimatePresence>
        <Flex justify='center' containerMargin='3em 0 0 0'>
          <Button primary rounded medium>
            Load more
          </Button>
        </Flex>
        <Footer />
      </div>
    </WithLoading>
  );
}

Home.propTypes = {
  articles: PropTypes.array.isRequired,
  isLoaded: PropTypes.bool.isRequired,
  shouldAnimateIn: PropTypes.bool.isRequired,
  handleInAnimation: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired
};

export default memo(Home);
