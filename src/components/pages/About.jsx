import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import { useRouteMatch } from 'react-router-dom';

import Footer from 'components/layout/Footer';

import { pageTransition, pageVariants } from 'styles/pageTransition';

import FlexMotion from 'components/FlexMotion';
import Flex from 'components/Flex';
import Grid from 'components/Grid';
import Text from 'components/Text';
import ExternalLink from 'components/ExternalLink';

import global from 'styles/global';

import IconBox from 'components/IconBox';

import { device } from 'styles/devices';

function About() {
  const match = useRouteMatch('/about');
  const isPageAbout = match.isExact;

  const tech = [
    {
      type: 'icon',
      name: 'JavaScript'
    },
    {
      type: 'icon',
      name: 'ReactJS'
    },
    {
      type: 'icon',
      name: 'NodeJS'
    },
    {
      type: 'icon',
      name: 'Redux'
    },
    {
      name: 'More',
      component: 'MoreStuff'
    }
  ];

  return (
    <Wrapper
      initial='initial'
      animate='in'
      exit='out'
      variants={pageVariants}
      transition={pageTransition}
    >
      <Grid columns='auto auto' align='start'>
        <Left />
        <Content>
          <Flex>
            <Text lineHeight='1.7'>
              I’m a <Hypertext>JavaScript Developer</Hypertext> + <Hypertext>UX Designer</Hypertext>
              . I help early stage startups. <br />
              <ExternalLink textDisplay='inline' textColor='brand' fontWeight='bold'>
                Email me
              </ExternalLink>{' '}
              if you think we should be working together on interesting projects.
            </Text>
          </Flex>
          <FlexMotion
            containerPadding='2.5em 0 0 0'
            initial='closed'
            variants={variants}
            animate={isPageAbout ? 'open' : 'closed'}
          >
            {tech.map((item, i) => (
              <IconBox key={i} type={item.type} text={item.name} />
            ))}
          </FlexMotion>
        </Content>
      </Grid>
      <Flex justify='center'>
        <Footer />
      </Flex>
    </Wrapper>
  );
}

const Content = styled.div`
  max-width: 630px;
`;

const Left = styled.div`
  @media ${device.tablet} {
    min-width: 20vw;
  }
`;

const Wrapper = styled(motion.div)`
  margin-top: 3em;
`;

const Hypertext = styled.span`
  color: ${global.color.brand};
  font-weight: bold;
`;

const variants = {
  open: {
    transition: { staggerChildren: 0.07, delayChildren: 0.2 }
  },
  closed: {
    transition: { staggerChildren: 0.05, staggerDirection: -1 }
  }
};

export default About;
