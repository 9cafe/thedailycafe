import styled from 'styled-components';

import color from '../styles/color';

const Icon = styled.svg`
	width: ${({ width }) => width}px;
	height: ${({ height }) => height}px;
	fill: ${({ fillColor }) => color[fillColor]};
	vertical-align: ${({ verticalAlign }) => verticalAlign};
`;

Icon.defaultProps = {
	fillColor: 'inherit',
	verticalAlign: 'middle'
};

export default Icon;
