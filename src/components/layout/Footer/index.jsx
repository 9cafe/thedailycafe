import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import Flex from '../../Flex';
import Text from '../../Text';
import IconLogo from '../../../icons/IconLogo';
import global from '../../../styles/global';

function Footer() {
  return (
    <Flex justify='center' align='center' containerMargin='4em 0 5em 0' direction='column'>
      <Text textColor='grey5' lineHeight={global.lineHeight.lg}>
        created by <Link to='/about'>Antonio Petre</Link>
      </Text>
      <Logo />
    </Flex>
  );
}

const Logo = styled(IconLogo)`
  margin-top: 3em;
  transform: scale(3);
  opacity: 0.13;
`;

const Link = styled(NavLink)`
  color: ${global.color.grey5};
  padding: 1em 0;
  &:hover {
    color: ${global.color.brand};
  }
`;

export default Footer;
