import React, { memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

// import { MyContext } from '../../../App.js';

import { device } from '../../../../styles/devices';
import global from '../../../../styles/global';

const Wrapper = styled.span`
  margin-right: ${global.spacing.sm}rem;

  @media ${device.tablet} {
    font-size: ${global.fontSize.sm}em;
    color: ${global.color.brand};
  }
`;

function ClickStats({ clicks, clicked }) {
  return <Wrapper>{clicks + clicked}</Wrapper>;
}

ClickStats.propTypes = {
  clicks: PropTypes.number.isRequired,
  clicked: PropTypes.number.isRequired
};

export default memo(ClickStats);
