import styled from 'styled-components';

import Flex from '../../Flex';

import { device } from '../../../styles/devices';
import global from '../../../styles/global';

export const Title = styled.h4`
  font-size: ${global.fontSize.lg}em;
  line-height: ${global.lineHeight.md}em;

  @media ${device.tablet} {
    font-size: ${global.fontSize.lg}em;
  }
`;

export const LinkContainerFlex = styled(Flex)`
  margin-top: 0.3em;

  @media ${device.tablet} {
    margin-top: ${global.spacing.xs}rem;
  }
`;

export const ShareLinkWrapper = styled.div`
  opacity: 0;
  align-self: center;

  ${LinkContainerFlex}:hover & {
    opacity: 1;
  }
`;

export const MetaDate = styled.div`
  color: ${global.color.grey};
  margin-right: ${global.spacing.sm}rem;

  font-size: 0.9em;
  font-weight: 400;

  @media ${device.laptop} {
    color: ${global.color.grey1};
    font-size: 0.9em;
    line-height: ${global.spacing.xl}em;
  }
`;

export const Wrapper = styled.article`
  margin-top: 4em;
  margin-bottom: 3em;
`;

export const IsMobile = styled(Flex)`
  align-self: center;

  @media ${device.tablet} {
    display: none;
  }
`;

export const IsDesktop = styled(Flex)`
  display: none;

  @media ${device.tablet} {
    display: inherit;
    line-height: ${global.lineHeight.xl2}rem;
    min-width: 20vw;
  }
  @media ${device.desktop} {
    min-width: 25vw;
  }
`;

export const Dot = styled.div`
  width: 0.23rem;
  height: 0.23rem;
  border-radius: 50%;
  padding: 0.2rem;
  margin-right: ${global.spacing.sm}rem;
  background: ${global.color.grey2};
  opacity: 0.5;
  align-self: center;
`;

export const SmartLinksContainer = styled.ul`
  display: flex;
  margin-top: ${global.spacing.xl}em;
`;
