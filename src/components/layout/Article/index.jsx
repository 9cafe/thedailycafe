import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { motion } from 'framer-motion';

import Grid from '../../Grid';
import Flex from '../../Flex';

import ArticleLink from '../../ArticleLink';
import SmartLink from '../../SmartLink';
import ShareLink from '../../ShareLink';
import ClickStats from './ClickStats';

// import FlexMotion from '../../components/FlexMotion';

import {
  Title,
  LinkContainerFlex,
  ShareLinkWrapper,
  MetaDate,
  Wrapper,
  IsMobile,
  IsDesktop,
  Dot,
  SmartLinksContainer
} from './partials';

import IconMore from '../../../icons/IconMore';

const variants = {
  open: {
    transition: { staggerChildren: 0.07, delayChildren: 0.2 }
  },
  closed: {
    transition: { staggerChildren: 0.05, staggerDirection: -1 }
  }
};

function Article({ article, handleClick, isLoaded }) {
  const [copyToClipboard, openInput] = useState({});
  const [tooltipText, showTooltip] = useState(false);

  const timeout = useRef();

  const handleInput = React.useCallback(id => {
    openInput({
      [id]: true
    });

    handleTooltip();
  }, []);

  const handleTooltip = () => {
    clearTimeout(timeout.current);
    showTooltip(true);

    timeout.current = setTimeout(() => {
      showTooltip(false);
    }, 3000);
  };

  return (
    <Wrapper>
      <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
        <Grid columns='auto auto' align='start'>
          <IsDesktop justify='flex-end'>
            <MetaDate justify='flex-end'>{article.meta.date}</MetaDate>
          </IsDesktop>
          <Flex direction={'column'}>
            <div>
              <Title>{article.meta.title}</Title>
            </div>
            {article.smartLinks && (
              <div>
                <SmartLinksContainer>
                  {article.smartLinks.map((item, i) => {
                    let title = item.title;
                    let link = item.link;
                    let isFirst = i === 0 ? true : false;

                    return <SmartLink key={i} title={title} link={link} isFirst={isFirst} />;
                  })}
                </SmartLinksContainer>
              </div>
            )}
          </Flex>
        </Grid>
        <Grid columns='none'>
          <motion.div variants={variants} animate={isLoaded ? 'open' : 'closed'}>
            {article.links.map(item => {
              const { id, title, link, description, clicks, clicked } = item;
              const articleId = article.id;

              return (
                <LinkContainerFlex align='start' key={id}>
                  <IsDesktop justify='flex-end'>
                    <ShareLinkWrapper>
                      <ShareLink link={link} title={title} handleInput={handleInput} />
                    </ShareLinkWrapper>
                    <ClickStats clicks={clicks} clicked={clicked} />
                    <Dot />
                  </IsDesktop>
                  <Flex align='start'>
                    <ArticleLink
                      articleId={articleId}
                      linkId={id}
                      link={link}
                      description={description}
                      copyToClipboard={copyToClipboard}
                      tooltipText={tooltipText}
                      handleTooltip={handleTooltip}
                      handleClick={handleClick}
                    >
                      {title}
                    </ArticleLink>
                    <IsMobile>
                      <IconMore />
                    </IsMobile>
                  </Flex>
                </LinkContainerFlex>
              );
            })}
          </motion.div>
        </Grid>
      </motion.div>
    </Wrapper>
  );
}

const MemoizedArticle = React.memo(Article);
export default MemoizedArticle;

Article.propTypes = {
  article: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  isLoaded: PropTypes.bool.isRequired
};
