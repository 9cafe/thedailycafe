import styled from 'styled-components';

import { device } from '../../styles/devices';

const offset = {
  mobileS: '1rem',
  mobileM: '1rem',
  mobileL: '1rem',
  tablet: '3rem',
  laptop: '5rem',
  laptopL: '8.7rem',
  desktop: '8.7rem'
};

const Layout = styled.main`
  padding: 0 ${offset.mobileS} 0 ${offset.mobileS};

  @media ${device.mobileS} {
    padding: 0 ${offset.mobileS} 0 ${offset.mobileS};
  }

  @media ${device.tablet} {
    padding: 0 ${offset.tablet} 0 ${offset.tablet};
  }

  @media ${device.laptop} {
    padding: 0 ${offset.laptop} 0 ${offset.laptop};
  }

  @media ${device.laptopL} {
    padding: 0 ${offset.laptopL} 0 ${offset.laptopL};
  }
`;
export default Layout;
