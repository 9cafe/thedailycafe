import React, { useState, memo } from 'react';
import PropTypes from 'prop-types';
import { NavLink, Link } from 'react-router-dom';
import { motion } from 'framer-motion';

import global from 'styles/global';

import ExternalLink from 'components/ExternalLink';
import Grid from 'components/Grid';
import Button from 'components/Button';
import NotificationsButton from 'components/NotificationsButton';

import Slogan from 'components/Slogan';

import IconLogo from 'icons/IconLogo';
import IconGoBack from 'icons/IconGoBack';

import { Menu, LeftContainer, LogoContainer, ButtonContainer, BackButton } from './partials';

const backButtonAnim = {
  open: { opacity: 1, x: '-75%' },
  close: { opacity: 0, x: '10%' }
};

const logoAnim = {
  open: { opacity: 1, x: 0 },
  close: { opacity: 0, x: '-140%' }
};

function NavBarContent({ isPageAbout, topView }) {
  const [notifications, setNotifications] = useState(false);

  return (
    <Grid columns='auto auto' justify='space-between'>
      <Grid columns='auto auto'>
        <LeftContainer>
          <LogoContainer
            animate={isPageAbout ? 'close' : 'open'}
            variants={logoAnim}
            initial={false}
          >
            <NavLink to='/'>
              <IconLogo />
            </NavLink>
          </LogoContainer>
          <ButtonContainer
            animate={isPageAbout ? 'open' : 'close'}
            variants={backButtonAnim}
            initial={false}
          >
            <NavLink to='/'>
              <BackButton outline normal rounded>
                <IconGoBack /> Back
              </BackButton>
            </NavLink>
          </ButtonContainer>
        </LeftContainer>
        <Slogan page={isPageAbout} topView={topView} />
      </Grid>
      <Grid columns='auto auto auto'>
        {!isPageAbout && (
          <NotificationsButton
            status={notifications}
            handleToggle={() => setNotifications(!notifications)}
          />
        )}
        <div style={{ display: 'flex' }}>
          <motion.div
            positionTransition
            style={{ display: 'flex', alignSelf: 'center' }}
            initial='collapsed'
            animate={isPageAbout ? 'open' : 'collapsed'}
            exit='collapsed'
            variants={{
              open: { x: 0 },
              collapsed: { x: 0 }
            }}
          >
            <NavLink to='/about' activeStyle={{ color: global.color.brand }}>
              <Menu>about me</Menu>
            </NavLink>
            <ExternalLink target='_blank' href='https://twitter.com/tonipetre'>
              <Menu strict>twitter</Menu>
            </ExternalLink>
          </motion.div>
          {isPageAbout && (
            <motion.div
              initial='collapsed'
              animate='open'
              exit='collapsed'
              transition={{ duration: 0, ease: 'easeOut' }}
              variants={{
                open: { opacity: 1, x: 0 },
                collapsed: { opacity: 0, x: '100%', duration: 0 }
              }}
            >
              <Button primary rounded medium bold style={{ marginLeft: '30px' }}>
                Say hello
              </Button>
            </motion.div>
          )}
        </div>
      </Grid>
    </Grid>
  );
}

NavBarContent.propTypes = {
  isPageAbout: PropTypes.bool.isRequired,
  topView: PropTypes.bool.isRequired
};

export default memo(NavBarContent);
