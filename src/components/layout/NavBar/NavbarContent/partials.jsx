import styled from 'styled-components';
import { motion } from 'framer-motion';

import { device } from 'styles/devices';
import global from 'styles/global';

import Button from 'components/Button';

export const Menu = styled.p`
  font-size: ${global.fontSize.lg}em;
  letter-spacing: 0.2px;
  padding: 0 10px 0 10px;
  line-height: 50px;

  ${({ strict }) =>
    strict &&
    `
    color: black;
  `};

  &:hover {
    color: ${global.color.pink};
  }
`;

export const LogoLink = styled.span`
  display: inline-block;

  @media ${device.tablet} {
    padding: ${global.spacing.sm}em 0;
  }
`;

export const LeftContainer = styled.div`
  min-width: 20vw;

  @media ${device.tablet} {
    padding: ${global.spacing.sm}em 0;
  }
  @media ${device.desktop} {
    padding: ${global.spacing.sm}em 0;
    min-width: 25vw;
  }
`;

export const LogoContainer = styled(motion.span)`
  display: inline-block;
`;

export const ButtonContainer = styled(motion.span)`
  display: inline-block;
`;

export const BackButton = styled(Button)`
	fill: ${global.color.brand};
	padding: 1.1em 2rem 1.1em 2rem;
  margin: 0.2px;

	/* line-height: ${global.lineHeight.lg}; */

	&:hover {
		fill: white;
	}
`;
