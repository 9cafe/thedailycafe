import React, { useState, useEffect, memo } from 'react';
import { useLocation } from 'react-router-dom';

import NavbarContent from './NavbarContent';

import { Nav } from './partials';

const navAnim = {
  top: { y: 0 },
  middle: { y: '-20%' }
};

function NavBar() {
  const [topView, setView] = useState(true);
  // const [hover, setHover] = useState(false);

  const location = useLocation();
  const isPageAbout = location.pathname.slice(1) === 'about' ? true : false;

  useEffect(() => {
    document.addEventListener('scroll', onScroll);
    onScroll();

    return () => {
      document.removeEventListener('scroll', onScroll);
    };
  }, []);

  const onScroll = () => {
    let scrollDistance = document.documentElement.scrollTop;

    if (scrollDistance > 80) {
      setView(false);
    }

    if (scrollDistance < 30) {
      setView(true);
    }
  };

  return (
    <Nav topView={topView} animate={topView ? 'top' : 'middle'} variants={navAnim} initial={false}>
      <NavbarContent isPageAbout={isPageAbout} topView={topView} />
    </Nav>
  );
}

export default memo(NavBar);
