import styled from 'styled-components';
import { motion } from 'framer-motion';

import { device } from 'styles/devices';

/*
 * Styled components
 */

export const Nav = styled(motion.nav)`
	padding-top: 1.8em;
	position: sticky;
	z-index: 5;
	top: 0;

	@media ${device.tablet} {
	}
`;
