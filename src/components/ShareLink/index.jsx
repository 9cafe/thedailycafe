import React, { useRef, memo } from 'react';

import { ButtonsGroup, ButtonLink } from './partials';

import IconTwitter from '../../icons/IconTwitter';
import IconFacebook from '../../icons/IconFacebook';
import IconCopy from '../../icons/IconCopy';

function ShareLink({ title, link, handleInput }) {
  const text = `${title} via — ${link}`;
  const facebookUrl = 'https://www.facebook.com/sharer/sharer.php?u=';
  const twitterUrl = `https://twitter.com/intent/tweet?text=${text}`;

  const inputRed = useRef(null);

  const copyToClipboard = e => {
    inputRed.current.select();
    document.execCommand('copy');
    handleInput(link);
  };

  return (
    <ButtonsGroup>
      <ButtonLink
        onClick={copyToClipboard}
        target='_blank'
        style={{ borderTopLeftRadius: 3, borderBottomLeftRadius: 3, marginRight: -1 }}
      >
        <IconCopy />
      </ButtonLink>
      <ButtonLink href={facebookUrl + link} target='_blank' style={{ marginRight: -1 }}>
        <IconFacebook />
      </ButtonLink>
      <ButtonLink
        href={twitterUrl}
        target='_blank'
        style={{ borderTopRightRadius: 3, borderBottomRightRadius: 3 }}
      >
        <IconTwitter />
      </ButtonLink>
      <input
        style={{ position: 'absolute', left: -1000, top: -1000 }}
        readOnly
        type='text'
        value={link}
        ref={inputRed}
      />
    </ButtonsGroup>
  );
}

export default memo(ShareLink);
