import styled from 'styled-components';

import global from '../../styles/global';

import { device } from '../../styles/devices';

export const ButtonsGroup = styled.div`
  display: none;
  margin-right: ${global.spacing.sm}rem;
  cursor: pointer;

  @media ${device.tablet} {
    display: flex;
    font-size: ${global.fontSize.lg2}em;
  }
`;

export const ButtonLink = styled.a`
  display: flex;
  width: ${global.spacing.md}em;
  height: ${global.spacing.md}em;

  align-items: center;
  justify-content: center;
  border: 1px solid ${global.color.grey3};

  &:hover {
    z-index: 2;
    border-color: ${global.color.brand};
  }
`;
