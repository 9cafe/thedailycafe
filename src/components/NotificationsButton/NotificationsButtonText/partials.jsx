import styled, { css, keyframes } from 'styled-components';

import global from '../../../styles/global';

const InAn = keyframes`
  0% {
    opacity: 0;
    transform: translateX(30px);
  }
  100% {
    opacity: 1;
    transform: translateX(0);
  }
`;

const OutAn = keyframes`
  0% {
    opacity: 1;
    transform: translateX(0);
  }
  100% {
    opacity: 0;
    transform: translateX(30px);
  }
`;

const SText = styled.span`
	position: absolute;
	font-size: 12px;
	letter-spacing: 0.2px;
	display: inline-block;
	white-space: nowrap;
	user-select: none;
`;

export const EnabledText = styled(SText)`
	color: ${global.color.brand};

	transform: translateY(10px);
	opacity: 0;
	transition: all 0.3s;

	${({ status }) =>
		status &&
		`
    transform: translateY(0);
    opacity: 1;
  `};
`;

export const DisabledText = styled(SText)`
	color: ${global.color.pink};

	opacity: 1;
	transition: all 0.3s;

	${({ status }) =>
		status &&
		`
    transform: translateY(-10px);
    opacity: 0;
  `};
`;

const LText = styled.span`
	position: absolute;
	top: 10px;
	right: -174px;
	padding: 10px 50px 10px 10px;
`;

export const SmallText = styled(LText)`
	${({ isHover }) =>
		isHover
			? css`
					animation: ${OutAn} 0.3s ease both;
			  `
			: css`
					animation: ${InAn} 0.3s 1s ease both;
			  `};
`;

export const LongText = styled(LText)`
	${({ isHover }) =>
		isHover
			? css`
					animation: ${InAn} 0.3s ease both;
			  `
			: css`
					animation: ${OutAn} 0.3s 1s ease both;
			  `};
`;
