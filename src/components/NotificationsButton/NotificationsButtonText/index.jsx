import React, { useState, useEffect, useRef } from 'react';

import { EnabledText, DisabledText, SmallText, LongText } from './partials';

function NotificationsButtonText({ status }) {
  const [isHover, setHover] = useState(true);

  let timeout = null;
  let mounted = useRef(false);

  useEffect(() => {
    mounted.current = true;

    setTimeout(() => {
      if (mounted.current) {
        setHover(false);
      }
    }, 2000);

    return () => (mounted.current = false);
  }, []);

  return (
    <div
      onMouseEnter={() => {
        setHover(true);
        clearTimeout(timeout);
      }}
      onMouseLeave={() => {
        timeout = setTimeout(() => {
          if (mounted.current) {
            setHover(false);
          }
        }, 2000);
      }}
    >
      <EnabledText status={status}>
        <LongText isHover={isHover}>disable notifications</LongText>
        <SmallText isHover={isHover}>enabled</SmallText>
      </EnabledText>
      <DisabledText status={status}>
        <LongText isHover={isHover}>enable notifications</LongText>
        <SmallText isHover={isHover}>disabled</SmallText>
      </DisabledText>
    </div>
  );
}

export default NotificationsButtonText;
