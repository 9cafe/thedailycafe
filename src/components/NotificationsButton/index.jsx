import React from 'react';
import styled from 'styled-components';

import color from '../../styles/color';

import NotificationsButtonText from './NotificationsButtonText';

const Label = styled.label`
	cursor: pointer;
`;

const CheckBoxLabel = styled.div`
	position: absolute;
	top: 22px;
	right: 0;
	width: 30px;
	height: 9px;
	border-radius: 15px;
	background: ${color.pinkbg};
	cursor: pointer;
	&::after {
		content: '';
		display: block;
		border-radius: 50%;
		width: 9px;
		height: 9px;
		margin: 0;
		background: ${color.pink};
		transition: 0.1s;
	}
`;
const CheckBox = styled.input`
	position: absolute;
	top: 22px;
	right: 0;
	margin: 0;
	opacity: 0;
	z-index: 1;
	border-radius: 15px;
	width: 30px;
	height: 9px;
	cursor: pointer;

	&:checked + ${CheckBoxLabel} {
		background: ${color.brandbg};
		&::after {
			content: '';
			display: block;
			border-radius: 50%;
			width: 9px;
			height: 9px;
			margin-left: 21px;
			background: ${color.brand};
			transition: 0.1s;
		}
	}
`;

const Wrapper = styled.div`
	min-width: 160px;
	min-height: 50px;
	margin-right: 15px;
	position: relative;
	overflow: hidden;
`;

const NotificationsButton = ({ status, handleToggle }) => {
	return (
		<Wrapper>
			<Label>
				<div>
					<CheckBox id='checkbox' type='checkbox' checked={status} onChange={handleToggle} />

					<CheckBoxLabel htmlFor='checkbox' />
				</div>

				<div>
					<NotificationsButtonText status={status} />
				</div>
			</Label>
		</Wrapper>
	);
};

export default NotificationsButton;
