import React from 'react';
import FlexMotion from './FlexMotion';
import Text from './Text';
import ExternalLink from './ExternalLink';

import IconJavascript from '../icons/IconJavascript';
import IconReactjs from '../icons/IconReactjs';
import IconNodejs from '../icons/IconNodejs';
import IconRedux from '../icons/IconRedux';
import { motion } from 'framer-motion';

const variants = {
  open: {
    opacity: 1
  },
  closed: {
    opacity: 0
  }
};

function IconBox({ type, text }) {
  const isIcon = type === 'icon' ? true : false;

  const isIconJavascript = text === 'JavaScript' ? true : false;
  const isIconReactjs = text === 'ReactJS' ? true : false;
  const isIconNodejs = text === 'NodeJS' ? true : false;
  const isIconRedux = text === 'Redux' ? true : false;

  if (isIcon) {
    return (
      <FlexMotion
        direction='column'
        align='center'
        variants={variants}
        containerMargin='0 3rem 0 0'
      >
        {isIconJavascript && <IconJavascript />}
        {isIconReactjs && <IconReactjs />}
        {isIconNodejs && <IconNodejs />}
        {isIconRedux && <IconRedux />}
        <Text fontSize='sm' textColor='grey3' containerMargin='1.3em 0 0 0'>
          {text}
        </Text>
      </FlexMotion>
    );
  }

  if (!isIcon) {
    return (
      <motion.div variants={variants}>
        <Text fontSize='md' textColor='grey3' lineHeight='1.5'>
          What about <br />
          other stuff? <br />
          <ExternalLink href='#' textColor='brand' fontSize='md' containerMargin='0.4em 0 0 0'>
            Say hello :)
          </ExternalLink>
        </Text>
      </motion.div>
    );
  }
}

export default IconBox;
