import styled, { css } from 'styled-components';
import global from '../styles/global';

const Button = styled.button`
	min-width: ${({ minWidth }) => minWidth};
	align-items: ${({ align }) => align};
	flex-direction: ${({ direction }) => direction};
	justify-content: ${({ justify }) => justify};
	margin: ${({ containerMargin }) => containerMargin};
	font-family: ${global.fontFamily.sans};
	cursor: pointer;
	padding: 1rem 2.1rem 1.1rem 2.1rem;

	${({ normal }) =>
		normal &&
		css`
			font-size: 0.8em;
		`};

	${({ medium }) =>
		medium &&
		css`
			font-size: 1em;
		`};

	${({ primary }) =>
		primary &&
		css`
			background: ${global.color.brand};
			color: white;

			&:hover {
				background: ${global.color.pink};
			}
		`};

	${({ outline }) =>
		outline &&
		css`
			background: transparent;
			border: 1px solid ${global.color.brand};
			color: ${global.color.brand};
			font-weight: ${global.fontWeight.medium};
			&:hover {
				background: ${global.color.brand};
				color: white;
			}
		`};

	${({ rounded }) =>
		rounded &&
		css`
			border-radius: 999px;
		`};

	${({ bold }) =>
		bold &&
		css`
			font-weight: ${global.fontWeight.bold};
		`};
`;

Button.defaultProps = {};

export default Button;
