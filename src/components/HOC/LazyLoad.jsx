import React, { Suspense, memo } from 'react';

const spinner = (
	<div className='cafeload'>
		<div></div>
	</div>
);

const LazyLoad = ({ component: Component, ...rest }) => (
	<>
		<Suspense fallback={spinner}>
			<Component {...rest} />
		</Suspense>
	</>
);

export default memo(LazyLoad);
