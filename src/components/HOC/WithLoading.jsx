import React, { memo } from 'react';

const WithLoading = props => {
	if (props.isLoaded) return { ...props.children };
	return (
		<div className='cafeload'>
			<div></div>
		</div>
	);
};

export default memo(WithLoading);
