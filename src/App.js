import React, { useState, useEffect, memo } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Layout from './components/layout/Layout';
import NavBar from './components/layout/NavBar';
import LazyLoad from './components/HOC/LazyLoad';
import './fonts/font.css';
import './styles/css/spinner.css';

import { device } from './styles/devices';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html {
    font-size: 16px;
    
    @media ${device.tablet} {
      font-size: 16px
	  }
    @media ${device.desktop} {
      font-size: 18px
	  }
    @media ${device.desktopL} {
      font-size: 22px
	  }
  }
`;

const Home = React.lazy(() => import('./components/pages/Home'));
const About = React.lazy(() => import('./components/pages/About'));

const LazyHome = props => <LazyLoad component={Home} {...props} />;
const LazyAbout = props => <LazyLoad component={About} {...props} />;

// export const actions = React.createContext();

function App() {
  const [data, setData] = useState([]);
  const [isLoaded, setLoaded] = useState(false);
  const [shouldAnimateIn, setAnimate] = useState(true);

  const handleClick = React.useCallback(
    (linkId, articleId) => {
      const newData = data.reduce((articles, article) => {
        if (article.id === articleId) {
          let newLinks = article.links.reduce((links, link) => {
            if (link.id === linkId) {
              let newLink = { ...link, clicked: link.clicked + 1 };
              links.push(newLink);
            } else {
              links.push(link);
            }

            return links;
          }, []);

          let newArticle = { ...article, links: [...newLinks] };
          articles.push(newArticle);
        } else {
          articles.push(article);
        }

        return articles;
      }, []);

      setData([...newData]);
    },
    [data]
  );

  const handleInAnimation = React.useCallback(() => {
    setAnimate(false);
  }, []);

  useEffect(() => {
    fetch('https://api.myjson.com/bins/psct0')
      .then(response => {
        return response.json();
      })
      .then(json => {
        setData(json);
        setLoaded(true);
      });
  }, []);

  return (
    <Router>
      <GlobalStyle />
      <Layout>
        <NavBar />
        <Switch>
          <Route
            exact
            path='/'
            component={() => (
              <LazyHome
                articles={data}
                isLoaded={isLoaded}
                shouldAnimateIn={shouldAnimateIn}
                handleClick={handleClick}
                handleInAnimation={handleInAnimation}
              />
            )}
          />
          <Route path='/about' component={LazyAbout} />
          <Route render={() => <p>Not Found</p>} />
        </Switch>
      </Layout>
    </Router>
  );
}

export default memo(App);
